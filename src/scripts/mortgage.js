"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var MortgageBanklist = function () {
    function MortgageBanklist() {
        var opts = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

        _classCallCheck(this, MortgageBanklist);

        var tpl = opts.tpl.replace(/\s{2,}/g, "");

        this.compiledTemplate = _.template(tpl);
        this.mountEl = $(opts.mountEl).get(0);
    }

    _createClass(MortgageBanklist, [{
        key: "render",
        value: function render(data) {
            this.mountEl.innerHTML = this.compiledTemplate(data);
        }
    }]);

    return MortgageBanklist;
}();

var MortgagePaymentCalc = function () {
    function MortgagePaymentCalc(banksData, formEl) {
        var _this = this;

        _classCallCheck(this, MortgagePaymentCalc);

        this.$form = $(formEl);

        if (!this.$form.length) {
            console.error('MortgageCalc constructor can\'t find given "formEl" in DOM!');
            return;
        }

        this.$form.on("submit", function (event) {
            event.preventDefault();
            return false;
        });

        this.banksListBuilder = new MortgageBanklist({
            mountEl: "#MortgageBanklistContainer",
            tpl: "\n             <div class=\"MortgageBanklist\">\n                <% _.forEach(banks, function(bank){ %>\n                    <div class=\"MortgageBanklist_row\">\n                        <div class=\"MortgageBanklist_cell MortgageBanklist_cell-img\"><img src=\"<%= bank.logo %>\" title=\"<%= bank.name %>\" alt=\"\u041B\u041E\u0413\u041E <%= bank.name %>\"></div>\n                        <div class=\"MortgageBanklist_cell MortgageBanklist_cell-description\">\n                            <h4 class=\"MortgageBanklist_header\"><span class=\"hidden-sm\">\u041F\u0435\u0440\u0432\u043E\u043D\u0430\u0447\u0430\u043B\u044C\u043D\u044B\u0439 </span>\u0432\u0437\u043D\u043E\u0441</h4>\n                            <p class=\"MortgageBanklist_text\">\n                                <% if(bank.firstFrom === bank.firstTo) {%>\n                                    <%= bank.firstFrom %>\n                                <%}else if(bank.firstTo === 100){%>\n                                    \u043E\u0442 <%= bank.firstFrom %>\n                                <%}else{%>\n                                    <%= bank.firstFrom %> - <%= bank.firstTo %>\n                                <%};%>%\n                            </p>\n                        </div>\n                        <div class=\"MortgageBanklist_cell MortgageBanklist_cell-rate\">\n                                <% if(bank.isHinted) {%>\n                                    <h4 class=\"MortgageBanklist_header MortgageBanklist_header-star\">\u0421\u0442\u0430\u0432\u043A\u0430</h4>\n                                <%}else{%>\n                                    <h4 class=\"MortgageBanklist_header\">\u0421\u0442\u0430\u0432\u043A\u0430</h4>\n                                <%};%>\n                            <p class=\"MortgageBanklist_text\" style=\"text-align:center;\">\n                                <% if(bank.rateFrom === bank.rateTo) {%>\n                                    <%= bank.rateFrom %>\n                                <%}else{%>\n                                    <%= bank.rateFrom %> - <%= bank.rateTo %>\n                                <%};%>%\n                        </p>\n                        </div>\n                        <div class=\"MortgageBanklist_cell\">\n                            <h4 class=\"MortgageBanklist_header\">\u0421\u0440\u043E\u043A \u043A\u0440\u0435\u0434\u0438\u0442\u0430 \u0434\u043E</h4>\n                            <p class=\"MortgageBanklist_text\"><%= bank.yearsTo %> \u043B\u0435\u0442</p>\n                        </div>\n                        <div class=\"MortgageBanklist_cell\">\n                            <h4 class=\"MortgageBanklist_header\">\u041F\u043B\u0430\u0442\u0435\u0436\u0438 \u0432 \u043C\u0435\u0441\u044F\u0446</h4>\n                            <p class=\"MortgageBanklist_text\"><%= bank.monthlyPayment %> \u20BD</p>\n                        </div>\n                    </div>\n                    <%}); %>\n                    <p class=\"MortgageBanklist_tipText\"><span class=\"MortgageBanklist_tipSign\">*</span>\u043F\u043E\u0434\u0440\u043E\u0431\u043D\u043E\u0441\u0442\u0438 \u043F\u0440\u0435\u0434\u043E\u0441\u0442\u0430\u0432\u043B\u0435\u043D\u0438\u044F \u0434\u0430\u043D\u043D\u044B\u0445 \u0443\u0441\u043B\u043E\u0432\u0438\u0439 \u0443\u0442\u043E\u0447\u043D\u044F\u0439\u0442\u0435 \u0443 \u043C\u0435\u043D\u0435\u0434\u0436\u0435\u0440\u0430 \u043F\u043E \u0438\u043F\u043E\u0442\u0435\u043A\u0435.</p>\n            </div>\n"
        });

        var onSliderHandlerChange = _.debounce(function (event) {
            if (!event.originalEvent) return false;
            _this.filterAndRenderBanks();
        }, 100);

        {
            var $formSliderBlock = this.$form.find(".MortgageCalc_option-firstPayment");

            var firstPaymentSliderInst = new MortgageSlider($formSliderBlock.find(".MortgageSlider"), {
                change: onSliderHandlerChange
            }, {
                slideEventCallback: function slideEventCallback() /*event, ui*/{
                    var selectedFirstPayment = firstPaymentSliderInst.getValue();
                    var selectedPrice = priceSliderInst.getValue();

                    if (selectedFirstPayment >= selectedPrice * 0.9) {
                        priceSliderInst.setHandlerPosition(Math.round(selectedFirstPayment * 1.15));
                    }
                },
                $counter: $formSliderBlock.find(".MortgageCalc_sum")
            });
        }

        {
            var _$formSliderBlock = this.$form.find(".MortgageCalc_option-price");

            var priceSliderInst = new MortgageSlider(_$formSliderBlock.find(".MortgageSlider"), {
                change: onSliderHandlerChange
            }, {
                slideEventCallback: function slideEventCallback() /*event, ui*/{
                    var selectedFirstPayment = firstPaymentSliderInst.getValue();
                    var selectedPrice = priceSliderInst.getValue();

                    if (selectedFirstPayment >= selectedPrice * 0.9) {
                        firstPaymentSliderInst.setHandlerPosition(Math.round(selectedPrice * 0.9));
                    }
                },
                $counter: _$formSliderBlock.find(".MortgageCalc_sum")
            });
        }

        {
            var _$formSliderBlock2 = this.$form.find(".MortgageCalc_option-duration");

            new MortgageSlider(_$formSliderBlock2.find(".MortgageSlider"), {
                change: onSliderHandlerChange
            }, {
                $counter: _$formSliderBlock2.find(".MortgageCalc_sum"),
                customCounterRender: function customCounterRender(v) {
                    if (v === 1) {
                        return "1 год";
                    } else {
                        return moment.duration(v, "years").humanize();
                    }
                }
            });
        }

        this.banksData = banksData;
    }

    _createClass(MortgagePaymentCalc, [{
        key: "testBank",
        value: function testBank(singleBankParams, opts) {
            var price = opts.price;
            var firstPay = opts.firstPay;
            var duration = opts.duration;

            // check by first pay party
            var firsPayPartyPercent = Math.round(firstPay / (price / 100));

            if (!singleBankParams.firstTo) singleBankParams.firstTo = 100;

            if (firsPayPartyPercent < singleBankParams.firstFrom || firsPayPartyPercent > singleBankParams.firstTo) return false;

            // check by pay duration range
            if (!_.inRange(duration, singleBankParams.yearsFrom, singleBankParams.yearsTo + 1)) return false;

            return true;
        }
    }, {
        key: "prepareDataForRender",
        value: function prepareDataForRender(data) {
            var opts = data.opts;

            data.banks = _.sortBy(data.banks, "rateFrom");

            for (var i = data.banks.length - 1; i >= 0; i--) {
                var bnkdt = data.banks[i];

                // prepare banks some datas for rendering
                if (!bnkdt.rateTo) bnkdt.rateTo = bnkdt.rateFrom;
                bnkdt.rateAverage = (bnkdt.rateFrom + bnkdt.rateTo) / 2;

                //calculate monthly payment
                {
                    var c = opts.price - opts.firstPay; // сумма кредита
                    var p = bnkdt.rateAverage; // годовая процентная ставка
                    var n = opts.duration * 12; // срок кредита в месяцах
                    var a = 1 + p / 1200; // знаменатель прогрессии
                    var k = Math.pow(a, n) * (a - 1) / (Math.pow(a, n) - 1); // коэффициент ежемесячного платежа
                    var sm = k * c; // ежемесячный платёж

                    bnkdt.monthlyPayment = parseInt(sm).toLocaleString("ru-RU");
                }
            }

            return data;
        }
    }, {
        key: "filterAndRenderBanks",
        value: function filterAndRenderBanks() {
            var _this2 = this;

            var allBanksParams = this.banksData;
            var selectedOpts = this.$form.serializeObject();

            // convert all selected values to 'INT' type
            selectedOpts = _.mapValues(selectedOpts, function (param) {
                return parseInt(param);
            });

            var acceptableBanks = [];

            // filter banks
            allBanksParams.forEach(function (bankParams) {
                if (_this2.testBank(bankParams, selectedOpts)) {
                    acceptableBanks.push(bankParams);
                }
            });

            // render banks
            var dataToRender = this.prepareDataForRender({
                banks: acceptableBanks,
                opts: selectedOpts
            });

            this.banksListBuilder.render(dataToRender);
        }
    }]);

    return MortgagePaymentCalc;
}();

var MortgageSlider = function () {
    function MortgageSlider(el, opts, addOpts) {
        var _this3 = this;

        _classCallCheck(this, MortgageSlider);

        if (!el) throw new Error("Element doesn't exist");

        var $el = $(el);

        this.customCounterRender = addOpts.customCounterRender ? addOpts.customCounterRender : false;
        this.$inp = $el.find("input");

        var $counter = addOpts.$counter;
        var minAllowedVal = parseInt(this.$inp.data("min-value"));
        var maxAllowedVal = parseInt(this.$inp.data("max-value"));
        var options = {
            classes: {
                "ui-slider-handle": "MortgageSlider_handle",
                "ui-slider-range": "MortgageSlider_progress"
            },
            min: minAllowedVal,
            max: maxAllowedVal,
            range: "min",
            value: this.$inp.val(),
            create: function create(event /*, ui*/) {
                _this3._setValue(_this3.$inp.val());

                $(event.target).find(".MortgageSlider_handle").insertBefore($(event.target).find(".MortgageSlider_progress"));
            },
            slide: function slide(event, ui) {
                _this3._setValue(ui.value);
                if (addOpts.slideEventCallback) addOpts.slideEventCallback();
            }
        };

        if (opts) {
            for (var key in opts) {
                options[key] = opts[key];
            }
        }

        var $track = $("<div class='MortgageSlider_track'></div>");
        $track.slider(options).appendTo($el);

        $counter.on("click", function (event) {
            if (event.currentTarget.getElementsByTagName("input").length) return false;

            var $popupInp = $("<input type='text' class='MortgageSlider_popupInp'>");
            $popupInp.val(_this3.$inp.val()).inputmask("integer", {
                groupSeparator: " ",
                autoGroup: true,
                allowMinus: false
            }).on("change", function (event) {
                event.preventDefault();
                var $tmpInp = $(event.currentTarget);
                var val = parseInt($tmpInp.val().split(" ").join(""));
                if (isNaN(val)) val = minAllowedVal;
                _this3.setHandlerPosition(val);
                if (addOpts.slideEventCallback) addOpts.slideEventCallback();
            });
            _this3.$counter.html($popupInp);
            $popupInp.trigger("focus");
        });

        this.minAllowedVal = minAllowedVal;
        this.maxAllowedVal = maxAllowedVal;
        this.$track = $track;
        this.$counter = $counter;
        this.$el = $el;
    }

    _createClass(MortgageSlider, [{
        key: "getValue",
        value: function getValue() {
            return this.$inp.val();
        }
    }, {
        key: "_setValue",
        value: function _setValue(v) {
            var _this4 = this;

            v = parseInt(v);
            this.$inp.val(v);

            window.requestAnimationFrame(function () {
                if (_this4.customCounterRender) {
                    var valToShow = _this4.customCounterRender(v);
                    _this4.$counter.html(valToShow);
                } else {
                    _this4.$counter.html(parseInt(v).toLocaleString("ru-RU"));
                }
            });
        }
    }, {
        key: "setHandlerPosition",
        value: function setHandlerPosition(v) {
            if (v < this.minAllowedVal) {
                v = this.minAllowedVal;
            }

            if (v > this.maxAllowedVal) {
                v = this.maxAllowedVal;
            }
            this._setValue(v);
            this.$track.slider("value", v);
        }
    }]);

    return MortgageSlider;
}();
//# sourceMappingURL=common.js.map
