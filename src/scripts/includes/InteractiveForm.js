$.validator.addMethod(
    "regex",
    function(value, element, regexp) {
        var re = new RegExp(regexp);
        return this.optional(element) || re.test(value);
    },
    "Поле заполнено не верно"
);
class InteractiveForm {
    constructor(opts) {
        this.$form = $(opts.el).eq(0);
        let $form = this.$form;
        if ($form.data("action")) {
            $form.prop('action', $form.data("action"));
        }

        $form.find('[name="phone"]').inputmask({
            mask: "8 999 999-99-99",
            showMaskOnHover: false,
        });

        $form.find('[name="captcha"]').inputmask({
            mask: "9999",
            showMaskOnHover: false
        });

        $form.find("[data-inputformat='num']").each((index, el) => {
            new Cleave(el, {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand',
                delimiter: ' ',
                numeralPositiveOnly: true,
            });
        });

        $form.find(".easydropdown").easyDropDown({
            cutOff: 6,
            nativeTouch: false,
            wrapperClass: 'easydropdown',
        });

        let validatorOpts = {
            rules: {
                phone: {
                    required: true,
                    regex: /8\s\d\d\d\s\d\d\d\-\d\d\-\d\d/
                },
                captcha: {
                    required: true,
                    regex: /\d\d\d\d/
                },

            },
            onfocusout: (el, event) => {
                $(el).valid();
            },
            onkeyup: false,
            onclick: false,
            focusCleanup: false,
            submitHandler: opts.submitHandler, //(form)=>{}
            errorPlacement: ($errorLabel, $el) => {

                /*if ($el.is(".b-custom-checkbox__input")) {
                    return true;
                } else {
                    $errorLabel.insertAfter($el);
                }*/

            },
        };

        if (opts.validatorParams) {
            $.extend(true, validatorOpts, opts.validatorParams);
        }

        this.validator = $form.validate(validatorOpts);

    }

    destroy() {
        this.validator.destroy();
        this.$form.find('input').inputmask('remove');
    }

}
