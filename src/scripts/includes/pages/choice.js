window.viewInitializers.choice = ($newView) => {
    $("#choicePageSpecificStyles").appendTo('head');
    $(".b-halfpage__link").one('click', function(event) {
        event.preventDefault();
        let $link = $(this);
        let $layer = $link.parent();

        let tl = new TimelineLite();
        tl
            .to(this, 0.3, {
                opacity: 0,
            })
            .set($layer, {
                className: "+=b-halfpage--active",
            })
            .addPause(0.42)
            .call(() => {
                if ($link.hasClass('b-halfpage__link--to-genplan')) {
                    TweenLite.to($(".b-halfpage__overlay:first"), 0.4, {
                        opacity: 0,
                        onComplete: () => {
                            window.requestAnimationFrame(() => {
                                let $bd = $("#bd");
                                $("#pagePreloader").addClass('b-preloader--transparent');
                                $(".b-logo__pic--dark").addClass('b-logo__pic--hidden');
                                $(".b-logo__pic--white").removeClass('b-logo__pic--hidden');
                                $("#choicePageSpecificStyles").remove();
                                if (getScreenSizeBreakpoint() != "xs") {
                                    $bd.addClass('white-page').attr('style', 'background: url(../img/genplan.jpg) center no-repeat;background-size: cover;');
                                } else {
                                    window.app.currentAppStates.nextFadeDuration = 0;
                                }
                                $link.trigger('click');
                            });
                        }
                    });
                } else {
                    $("#choicePageSpecificStyles").remove();
                    $link.trigger('click');
                }
            });
    });

    $newView.one('replace', (event) => {
        $("#choicePageSpecificStyles").remove();
    });
};

/*
app.setLocation($link.attr('href'));
*/
