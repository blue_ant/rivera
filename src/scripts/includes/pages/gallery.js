window.viewInitializers.gallery = ($newView) => {
    let $wrap = $newView.find(".galery_1:first");
    let $slider = $wrap.find(".galery_list:first");
    let $window = $(window);
    let popupGallery;

    $slider.slick({
        slidesToShow: 1,
        centerMode: true,
        variableWidth: true,
        centerPadding: 0,
        infinite: false,
        prevArrow: $wrap.find(".galery_str_l:first"),
        nextArrow: $wrap.find(".galery_str_r:first"),
    });

    $window.on('resize.galleryPage orientationchange.galleryPage', _.debounce((event) => {
        $slider.slick('slickGoTo', $slider.slick('slickCurrentSlide'));
    }, 100));

    $slider.on('click', '.wrap', (event) => {
        event.preventDefault();
        event.stopPropagation();
        $.ajax({
                url: `http://pirogovo-riviera.ru/about/gallery/?get_items=${event.currentTarget.dataset.ajaxPopupGallery}`,
            })
            .done((response) => {
                if (!popupGallery) {
                    popupGallery = new PopupGallery(response.slides);
                }else{
                    popupGallery.fillGallery(response.slides);
                }
                
                popupGallery.show();
            });

        return false;
    });

    $newView.one('replace', (event) => {
        $window.off(".galleryPage");
        $slider.slick('unslick');
        if (popupGallery) {
            popupGallery.destroy();
            popupGallery = null;
        }
    });
};
