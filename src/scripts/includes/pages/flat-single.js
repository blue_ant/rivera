window.viewInitializers["flat-single"] = ($newView) => {
    let scalePlanImgs = () => {
        let $imgsContainer = $newView.find('.flat_plan:first');
        let $inners = $imgsContainer.find('.tab-pane');
        let maxAllowedHeight = $(document.body).height() - 424;
        let currentHeight = $imgsContainer.height();
        let screenSizeBreakpoint = getScreenSizeBreakpoint();
        if (screenSizeBreakpoint === "xs" || screenSizeBreakpoint === "sm") return;
        let tl = new TimelineLite();
        tl
            .set($inners, { clearProps: "all" })
            .call(() => {
                if (currentHeight > maxAllowedHeight) {
                    TweenLite.set($inners, { scale: (_.ceil((maxAllowedHeight / currentHeight), 2)) - 0.1});
                }
            });

    };

    scalePlanImgs();

    let singleFlatForm = new InteractiveForm({
        el: "#singleFlatForm",
        submitHandler: modalFormHandler
    });

    let $flatvarsTitlesBlock = $newView.find(".b-flatvars-titles:first");
    let $flatvarsTitlesSlider = $flatvarsTitlesBlock.find(".b-flatvars-titles__wrap:first");
    let $underLine = $newView.find("#flatvarsTitleUnderline");

    let sliderOpts = {
        centerMode: true,
        draggable: false,
        variableWidth: false,
        centerPadding: '0px',
        infinite: false,
        slidesToShow: 3,
        focusOnSelect: true,
        //initialSlide: 1,
        prevArrow: $flatvarsTitlesBlock.find('.b-flatvars-titles__arrow--prev:first'),
        nextArrow: $flatvarsTitlesBlock.find('.b-flatvars-titles__arrow--next:first'),
    };

    var $3dModal = $('#3d-modal');

    $('[data-3d]').on('click', function(e) {
        let $this = $(this);
        let href  = $this.data('3d');

        e.preventDefault();

        setTimeout(function() {
            $3dModal.find('[data-modal-3d]').html('<iframe style="width: 100%; height: 100%" seamless="seamless" src="/3d/' + href + '"></iframe>');
            $3dModal.modal('show');
        }, 200);
    });

    if ($flatvarsTitlesSlider.find(".b-flatvars-titles__item").length === 2) {
        sliderOpts.slidesToShow = 1;
        sliderOpts.infinite = true;
        //sliderOpts.initialSlide = 1;
    }

    $flatvarsTitlesSlider
        .on('init', (event, slick) => {
            let $currentTitleLink = $(slick.$slides).filter(".slick-center").find("a");
            $underLine.width($currentTitleLink.width());
        })
        .slick(sliderOpts)

        .on('beforeChange', (event, slick, currentSlide, nextSlide) => {
            let $activeTitle = $flatvarsTitlesSlider.find('.b-flatvars-titles__item').eq(nextSlide).find('a');
            let tl = new TimelineLite();
            tl
                .to($underLine, 0.2, { y: "3px" })
                .call(() => {
                    TweenLite.to($underLine, 0.4, {
                        width: $activeTitle.width() - 4 + "px",
                    });
                })
                .to($underLine, 0.2, { y: "0px" });

            $activeTitle.trigger('click');
        });

    $(window).on('resize.singleFlatPage orientationchange.singleFlatPage', _.debounce((event) => {
        scalePlanImgs();
    }, 90));

    $newView.one('replace', (event) => {
        $(window).off('.singleFlatPage');
        try {
            singleFlatForm.destroy();
        } catch (e) {}
        $flatvarsTitlesSlider.slick('unslick');
    });
};
