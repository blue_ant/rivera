window.viewInitializers.hypothec = ($newView) => {
    $newView.find(".all_tarifs").on('click', function(event) {
    	event.preventDefault();
    	let $el = $(event.target);
    	$el.parent().parent().find(".td_wrap__hidden").slideDown();
    	TweenLite.to($el, 0.4, {autoAlpha: 0 , scale: 0.4});
    });

    let hypothecForm = new InteractiveForm({
        el: "#hypothecForm",
        submitHandler: modalFormHandler,
    });

    $newView.one('replace', (event) => {
        hypothecForm.destroy();
    });
};
