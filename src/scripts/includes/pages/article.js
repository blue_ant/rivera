window.viewInitializers.article = ($newView) => {

    let $imgModal = $newView.find("#articleBigImage");

    $imgModal.on('show.bs.modal', (event) => {
        centerModals($(event.target));
    });

    $(window).on('resize.articlePage', centerModals);

    $newView.one('replace', () => {
        $imgModal.removeClass('fade').modal("hide");
        $imgModal.modal = null;
        $(window).off('.articlePage');
    });
};