window.viewInitializers.location = ($newView) => {

    let routeMarkerPic = {
        url: "/img/location/route-marker.png",
        anchor: new google.maps.Point(22, 22),

    };
    let directionsDisplay = new google.maps.DirectionsRenderer({
        polylineOptions: {
            strokeColor: "#21a2db",
            strokeWeight: 3,
        },
        markerOptions: {
            icon: routeMarkerPic,
            zIndex: 0,
        },

    });
    let directionsService = new google.maps.DirectionsService();
    let buildingCoords = {
        lat: 55.986692,
        lng: 37.728029
    };

    function bindMapRouteRenderOnClick(mapInstance, travelMode) {
        directionsDisplay.setMap(null);
        google.maps.event.clearListeners(mapInstance, 'click');
        mapInstance.setOptions({
            draggableCursor: 'pointer',
        });
        google.maps.event.addListener(mapInstance, 'click', function(event) {

            let request = {
                origin: event.latLng,
                destination: buildingCoords,
                travelMode: google.maps.TravelMode[travelMode],
            };
            directionsService.route(request, function(response, status) {
                if (status == google.maps.DirectionsStatus.OK) {
                    //window.console.log(response.routes[0].legs[0].end_location);
                    directionsDisplay.setDirections(response);
                    directionsDisplay.setMap(mapInstance);
                } else {
                    alert("Невозможно проложить маршрут из этой точки.");
                }
            });

            return false;
        });

        alert('Отметьте Ваше текущее местоположение на карте');
    }

    let map = new google.maps.Map(document.getElementById('locationMap'), {
        zoom: 12,
        center: buildingCoords,
        streetViewControl: false,
        styles: window.googleMapStyles,
        mapTypeControl: false,
        fullscreenControl: false,
        zoomControlOptions: {
            position: google.maps.ControlPosition.RIGHT_CENTER
        }
    });

    directionsDisplay.setMap(map);

    let markerPic = {
        url: "/img/location/point.svg",
        scaledSize: new google.maps.Size(70, 70),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(38, 70),


    };
    let mainMarker = new google.maps.Marker({
        position: buildingCoords,
        map: map,
        icon: markerPic,
        iconsType: "unbeatable",
        zIndex: 200,
    });

    /*let infowindow = new google.maps.InfoWindow({
        content: `hello world`,
        position: buildingCoords,
    });

    mainMarker.addListener('click', function() {
        infowindow.open(map, mainMarker);
    });*/

    $newView.find('[data-toggle="tooltip"]').tooltip();

    $newView.find('#showCarRouteOnMapBtn').on('click', function(event) {
        event.preventDefault();
        bindMapRouteRenderOnClick(map, "DRIVING");
    });

    let contactsForm = new InteractiveForm({
        el: $newView.find("#contactsPageForm"),
        submitHandler: modalFormHandler
    });

    $newView.one('replace', (event) => {
        map = null;
        $newView.find('[data-toggle="tooltip"]').tooltip("destroy");
        contactsForm.destroy();
    });

};
