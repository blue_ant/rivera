window.viewInitializers.plans = ($newView) => {
    let $slider = $("#plansCarousel");
    let $blocks = $slider.find(".item");

    $blocks.matchHeight({ remove: 1 });

    setTimeout(() => {
        $blocks.matchHeight();
        $blocks.css('opacity', '1');
    }, 100);

    $slider.slick({
        mobileFirst: true,
        slidesToShow: 1,
        infinite: false,
        nextArrow: $newView.find('.str_r:first'),
        prevArrow: $newView.find('.str_l:first'),
        responsive: [{
            breakpoint: 767,
            settings: "unslick"
        }]
    });

    $(window).on('resize.plansPage orientationchange.plansPage', _.debounce(() => {
        if (getScreenSizeBreakpoint() === "xs" && !$slider.hasClass('slick-initialized')) {
            $slider.slick("reinit");
        }
    }, 100));

    $newView.one('replace', () => {
        $(window).off(".plansPage");
        $slider.slick('unslick');
    });
};
