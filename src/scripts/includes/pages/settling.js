window.viewInitializers.settling = ($newView) => {

    let $contactsModal = $newView.find("#contactsModal");
    $contactsModal.on('show.bs.modal', function(e) {
        centerModals($(this));
    });

    $newView.one('replace', (event) => {
        if ($contactsModal.hasClass('in')) {
            $contactsModal.removeClass('fade').modal("hide");
        }
    });
};
