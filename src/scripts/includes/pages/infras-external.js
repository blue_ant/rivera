window.viewInitializers["infras-external"] = ($newView) => {

    let buildingCoords = {
        lat: 55.986123,
        lng: 37.721731
    };

    let map = new google.maps.Map(document.getElementById('infrasMap'), {
        zoom: 12,
        center: { lat: buildingCoords.lat + 0.01, lng: buildingCoords.lng },
        streetViewControl: false,
        styles: window.googleMapStyles,
        mapTypeControl: false,
        fullscreenControl: false,
        zoomControlOptions: {
            position: google.maps.ControlPosition.RIGHT_CENTER
        }
    });



    let markers = _.sortBy(window.ifrasObjs, function(o) {
        return o.coords.lat;
    });

    let insertedMarkers = [];
    let defaultMarkerSizes = {
        xPopupOffset: 14,
        xMarkerScale: 87,
        yMarkerScale: 66,
        xAnchorOffset: 59,
        yAnchorOffset: 61,
    };
    if (getScreenSizeBreakpoint() === "xs") {
        for (let key in defaultMarkerSizes) {
            defaultMarkerSizes[key] *= 0.77;
        }
    }
    let infowindow = new google.maps.InfoWindow({
        pixelOffset: new google.maps.Size(defaultMarkerSizes.xPopupOffset, 0)
    });
    for (let i = 0; i < markers.length; i++) {

        let image = {
            url: markers[i].ico,
            scaledSize: new google.maps.Size(defaultMarkerSizes.xMarkerScale, defaultMarkerSizes.yMarkerScale),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(defaultMarkerSizes.xAnchorOffset, defaultMarkerSizes.yAnchorOffset)
        };

        let marker = new google.maps.Marker({
            position: markers[i].coords,
            map: map,
            icon: image,
            iconsType: markers[i].type,
        });
        marker.addListener('click', () => {
            infowindow.setContent(markers[i].title);
            infowindow.open(map, marker);
        });

        insertedMarkers.push(marker);
    }

    let mainPin = {
        url: "/img/location/point.svg",
        scaledSize: new google.maps.Size(70, 70),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(38, 70)

    };
    let mainMarker = new google.maps.Marker({
        position: buildingCoords,
        map: map,
        icon: mainPin,
        iconsType: "unbeatable",
    });
    insertedMarkers.push(mainMarker);


    $(".b-map-btns__link").on('click', (event) => {
        event.preventDefault();

        $(event.currentTarget).parent().toggleClass('sel');

        let $activeTags = $(".menuLocation:first .sel a");
        if (!$activeTags.length) {
            for (let i = insertedMarkers.length - 1; i >= 0; i--) {
                insertedMarkers[i].setVisible(true);
            }
        } else {
            let activeTypes = [];
            $activeTags.each((index, el) => {
                activeTypes.push(parseInt(el.dataset.iconsType));
            });

            for (let i = insertedMarkers.length - 1; i >= 0; i--) {
                if (insertedMarkers[i].iconsType === "unbeatable") {
                    continue;
                } else if (_.includes(activeTypes, insertedMarkers[i].iconsType)) {
                    insertedMarkers[i].setVisible(true);
                } else {
                    insertedMarkers[i].setVisible(false);
                }
            }
        }

    });


    let $tooltiped = $newView.find('[data-toggle="tooltip"]');
    $tooltiped.tooltip({ trigger: "hover" });
    if (device.mobile) {
        $tooltiped.on('shown.bs.tooltip', (event) => {
            setTimeout(() => {
                $(event.target).tooltip("hide");
            }, 2000);
        });
    }

    $newView.one('replace', ( /*event*/ ) => {
        map = insertedMarkers = null;
        $tooltiped.tooltip("destroy");
    });
};