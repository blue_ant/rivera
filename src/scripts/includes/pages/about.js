window.viewInitializers.about = ($newView) => {
    let $videoModals = $newView.find("#aboutPageVideoModal,#aboutPageVideoModalSecond");

    $videoModals.on('hide.bs.modal', (event) => {
        window.requestAnimationFrame(() => {
            let $iframe = $(event.target).find('iframe:first');
            $iframe.attr("src", $iframe.attr("src"));
        });

    }).on('show.bs.modal', function() {
        centerModals($(this));
    });

    $(window).on('resize.aboutPage', centerModals);

    $newView.one('replace', () => {
        $videoModals.removeClass('fade').modal("hide");
        $videoModals.modal = null;
        $(window).off('.aboutPage');
    });

};