window.viewInitializers["infras-internal"] = ($newView) => {

    let mapFitInstance = new Fitblock($newView.find('.b-infras-inter:first').get(0));

    $(window).on('resize.infrasInter orientationchange.infrasInter', _.debounce(() => {
        mapFitInstance.refresh();
    }, 100));

    let $markers = $newView.find('.point');
    let $filterLeafs = $newView.find(".menuLocation li");
    let $filterBtns = $newView.find(".menuLocation [data-pins-toggle]");


    let $tooltiped = $markers.add($filterLeafs);
    $tooltiped.tooltip({ trigger: "hover" });
    if(device.mobile){
        $tooltiped.on('shown.bs.tooltip', (event)=> {
            setTimeout(()=>{
                $(event.target).tooltip("hide");
            },2000);
        });
    }


    $filterBtns.on('click', (event) => {
        event.preventDefault();
        let $btn = $(event.currentTarget);
        let $pins = $markers;

        $btn.toggleClass('active-btn').parent().toggleClass('sel');

        let $activeBtns = $newView.find(".active-btn");
        if (!$activeBtns.length) {
            TweenLite.to($pins, 0.4, {
                autoAlpha: 1,
            });
        } else {
            let activePinsGroups = [];
            $activeBtns.each(function(index, el) {
                activePinsGroups.push(el.dataset.pinsToggle);
            });

            let $activePins = $pins.filter(function(index, el) {
                return _.includes(activePinsGroups, el.dataset.pinsGroup);
            });

            let $nonActivePins = $pins.filter(function(index, el) {
                return !_.includes(activePinsGroups, el.dataset.pinsGroup);
            });

            let tl = new TimelineLite();
            tl
                .addLabel("animeStart")
                .to($activePins, 0.4, {
                    autoAlpha: 1,
                }, "animeStart")
                .to($nonActivePins, 0.4, {
                    autoAlpha: 0,
                }, "animeStart");

        }

    });


    $newView.one('replace', (event) => {
        $(window).off(".infrasInter");
        $tooltiped.tooltip("destroy");
    });
};
