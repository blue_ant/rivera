window.viewInitializers.genplan = ($newView) => {

    let getCirclesSizes = () => {
        let isSm = getScreenSizeBreakpoint() === "xs";
        let result = {
            circleRadius: isSm ? 12 : 17,
            circleWidth: isSm ? 4 : 6,
            bigCircleRadius: isSm ? 19 : 25,
            bigCircleWidth: isSm ? 6 : 16,
        };

        return result;
    };

    let getCircleColor = (el) => {
        return el.className.split("--color-")[1].split(/\b/)[0] || "orange";
    };

    let sizes = getCirclesSizes();
    let circles = [];
    let bigCircles = [];

    $newView.find('.point').each((index, el) => {

        try {
            let $pin = $(el);
            let percents = $pin.data("percents-to-release");
            let circleEl = $pin.find('.genplan__circle:first').get(0);

            let circ = Circles.create({
                id: circleEl.id,
                radius: sizes.circleRadius,
                width: sizes.circleWidth,
                value: $pin.data("percents-to-release"),
                colors: ['#CDCDCD', `${percents ? ('url(#linear-'+ getCircleColor(el) +')') : '#CDCDCD'}`],
                duration: null,
                wrpClass: 'circles-wrp',
                text: null,
                valueStrokeClass: 'circles-valueStroke',
                maxValueStrokeClass: 'circles-maxValueStroke',
                styleWrapper: true,
            });
            circles.push(circ);
        } catch (err) {
            console.log(err);
        }
    });

    $newView.find('.point_ready').each((index, el) => {

        try {
            let $pin = $(el);
            let circleEl = $pin.find('.genplan__circle:first').get(0);

            getCircleColor(el);

            let circ = Circles.create({
                id: circleEl.id,
                radius: sizes.bigCircleRadius,
                width: sizes.bigCircleWidth,
                value: $pin.data("percents-to-release"),
                colors: ['#CDCDCD', `url(#linear-${getCircleColor(el)})`],
                duration: null,
                wrpClass: 'circles-wrp',
                text: null,
                valueStrokeClass: 'circles-valueStroke',
                maxValueStrokeClass: 'circles-maxValueStroke',
                styleWrapper: true,
            });

            bigCircles.push(circ);
        } catch (err) {
            console.log(err);
        }
    });



    $("#pagePreloader").removeClass('b-preloader--transparent');

    setTimeout(() => {
        $("#bd").removeAttr('style');
    }, 1000);
    let mapFitInstance = new Fitblock($newView.find('#genplanBg').get(0));

    $(window).on('resize.genplanPage orientationchange.genplanPage', _.debounce(() => {
        mapFitInstance.refresh();
        let sizes = getCirclesSizes();
        for (let i = circles.length - 1; i >= 0; i--) {
            circles[i].updateRadius(sizes.circleRadius);
            circles[i].updateWidth(sizes.circleWidth);

        }
        for (let i = bigCircles.length - 1; i >= 0; i--) {
            bigCircles[i].updateRadius(sizes.bigCircleRadius);
            bigCircles[i].updateWidth(sizes.bigCircleWidth);
        }
    }, 100));

    $newView.find(".numder_point_ready").on('click', function(event) {
        
        let $point = $(event.currentTarget);
        let houseID = $point.data('house-id');
        // if small screen then go straith to the filter page


        if ($point.parent().parent().hasClass("point_ready--sale-office")) {

            return false;

        } else if (getScreenSizeBreakpoint() === "xs" || device.mobile() /* || houseID === "38|54"*/ ) {

            window.localStorage.setItem("filterFormParams", `
[{
    "name": "house[]",
    "value": "${houseID}"
}]
`);
            $('<a href="/flats/filter/" class="hidden"></a>').appendTo($newView).trigger("click");
        }
    });
    $newView.one('replace', () => {
        $(window).off(".genplanPage");
    });

};