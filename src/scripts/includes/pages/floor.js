window.viewInitializers.floor = ($newView) => {

    let $cells = $newView.find('svg').find('g');
    $cells.each((index, el) => {
        let $el = $(el);

        let data = window.sectionFlatsForFloorPage[index];
        let isSale = data.status !== "busy";

        if (!isSale) {
            $el.css({
                "fill-opacity": '1',
                cursor: 'not-allowed',
            });
        }

        $el.uitooltip({
            items: "[id]",
            track: true,
            hide: false,
            show: false,
            tooltipClass: "b-floor-plan__popup",
            content: `\
<div class="row">
  <div class="b-floor-plan__section-num">${isSale ? "№" + data.num : "<span class='text-danger'>ПРОДАНО</span>"}</div>
  <div class="b-floor-plan__section-type">${data.bedrooms ? "спален: " + data.bedrooms : "студия"}</div>
</div>
<div class="row">
  <div class="b-floor-plan__section_label">площадь</div>
  <div class="b-floor-plan__section-square"><span class="big">${data.area}</span> м<sup>2</sup></div>
</div>`
        });

        if(isSale){
          $el.one('click', event => {
              event.preventDefault();
              let $tmpLink = $(`<a href='${data.href}'></a>`).appendTo($newView).trigger("click");
          });
        }
    });
};
