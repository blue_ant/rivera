window.viewInitializers.diary = ($newView) => {
    let $wrap = $(".galery_1:first");
    let $slider = $wrap.find(".galery_list:first");
    let $window = $(window);
    let popupGallery;

    $slider.slick({
        slidesToShow: 1,
        centerMode: true,
        variableWidth: true,
        centerPadding: 0,
        infinite: false,
        prevArrow: $wrap.find(".galery_str_l:first"),
        nextArrow: $wrap.find(".galery_str_r:first"),
    });

    $window.on('resize.diaryPage orientationchange.diaryPage', _.debounce((event) => {
        $slider.slick('slickGoTo', $slider.slick('slickCurrentSlide'));
    }, 100));

    $slider.on('click', '.slick-current > .wrap', (event) => {
        event.preventDefault();
        event.stopPropagation();
        $.ajax({
                url: `/about/diary/?get_items=${event.currentTarget.dataset.ajaxPopupGallery}`,
            })
            .done((response) => {
                if (!popupGallery) {
                    popupGallery = new PopupGalleryWithDetails(response.slides);
                } else {
                    popupGallery.fillGallery(response.slides);
                }
                popupGallery.fillGallery(response.slides);
                popupGallery.show();
            });

        return false;
    });


    let $webcamModals = $newView.find(".modal");

    $webcamModals.find('iframe').each((index, el) =>{
        let $iframe = $(el);
        $iframe.attr('src',decodeURIComponent($iframe.attr('src')));
        //window.console.log(decodeURIComponent($iframe.attr('src')));
        //$(el).prop('src', decodeURIComponent(el.src));
        //el.src = decodeURIComponent(el.src);
    });

    $webcamModals.on('hide.bs.modal', (event) => {
        window.requestAnimationFrame(() => {
            let $iframe = $(event.currentTarget).find('iframe:first');
            $iframe.attr("src", $iframe.attr("src"));
        });

    }).on('show.bs.modal', function(e) {
        centerModals($(this));
    });

    $(window).on('resize.diaryPage', centerModals);

    $newView.one('replace', (event) => {
        $window.off(".diaryPage");
        $slider.slick('unslick');
        if (popupGallery) {
            popupGallery.destroy();
            popupGallery = null;
        }
    });
};
