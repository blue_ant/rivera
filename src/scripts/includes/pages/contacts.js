window.viewInitializers.contacts = ($newView) => {

    let mainOfficeCoords = {
        lat: 55.750322, 
        lng: 37.569361
    };

    let secondaryOfficeCoords = {
        lat: 55.98427899364282,
        lng: 37.71758794784546
    };



    let map = new google.maps.Map(document.getElementById('locationMap'), {
        zoom: 9,
        center: mainOfficeCoords,
        streetViewControl: false,
        styles: window.googleMapStyles,
        mapTypeControl: false,
        fullscreenControl: false,
        zoomControlOptions: {
            position: google.maps.ControlPosition.RIGHT_CENTER
        }
    });

    let mainPin = {
        url: "/img/contact/point_yellow.svg",
        scaledSize: new google.maps.Size(70, 70),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(38, 70)

    };
    let mainMarker = new google.maps.Marker({
        position: mainOfficeCoords,
        map: map,
        icon: mainPin,
        iconsType: "unbeatable",
    });

    let secondPin = {
        url: "/img/contact/point_blue.svg",
        scaledSize: new google.maps.Size(70, 70),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(38, 70)

    };
    let secondMarker = new google.maps.Marker({
        position: secondaryOfficeCoords,
        map: map,
        icon: secondPin,
        iconsType: "unbeatable",
    });

    let contactsForm = new InteractiveForm({
        el: "#contactsPageForm",
        submitHandler: modalFormHandler
    });

    $newView.one('replace', (event) => {
        map = null;
        contactsForm.destroy();
    });

};
