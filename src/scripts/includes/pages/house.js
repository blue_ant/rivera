window.viewInitializers.house = $newView => {

    let mapFitInstance = new Fitblock($newView.find('#houseBg').get(0));

    $(window).on('resize.housePage orientationchange.housePage', _.debounce(() => {
        mapFitInstance.refresh();
    }, 100));

    let $sections = $newView.find("path");

    for (var i = 0; i < window.cellsForHousePage.length; i++) {
        let s = window.cellsForHousePage[i];
        let $svgPath = $sections.eq(i);
        $svgPath.uitooltip({
            track: true,
            items: "[fill]",
            hide: false,
            show: false,
            tooltipClass: "floor_info",
            content: `\
<div class="floor_number">${s.floor} этаж</div>
<div class="section_number">${s.section} секция</div>
<div class="flat_type type_1">студия <span class="count">${s.rooms0}</span></div>
<div class="flat_type type_2">1-комнатная <span class="count">${s.rooms1}</span></div>
<div class="flat_type type_3">2-комнатная <span class="count">${s.rooms2}</span></div>
<div class="flat_type type_4">3-комнатная <span class="count">${s.rooms3}</span></div>`
        });

        $svgPath.one('click', event => {
            event.preventDefault();
            let $tmpLink = $(`<a href='${s.href}'></a>`).appendTo($newView).trigger("click");
        });
    }

    $newView.one('replace', event => {
        $(window).off(".housePage");
    });
};