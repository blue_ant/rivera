window.viewInitializers.flats = ($newView) => {

    let preserveFiltersParams = (opts) => {
        let $inputs = opts.inputs;
        if (opts.skip) {
            $inputs = $inputs.filter(function(index, element) {
                return (_.indexOf(opts.skip, element.name) === -1);
            });
        }
        let inputsArr = $inputs.serializeArray();

        localStorage.setItem(opts.storageID, JSON.stringify(inputsArr));
    };

    let restoreFiltersParams = (opts) => {
        let $form = opts.form;
        let storageID = opts.storageID;

        let preservedFiltersData = localStorage.getItem(storageID);
        if (!preservedFiltersData) return;

        let statesArr = JSON.parse(preservedFiltersData);
        statesArr = _.groupBy(statesArr, "name");

        if (statesArr.area_from) {
            let $squareSlider = $form.find("#squareSlider");
            let optsForSquareSlider = $squareSlider.data('ranges');
            optsForSquareSlider.valueMin = statesArr.area_from[0].value;
            optsForSquareSlider.valueMax = statesArr.area_to[0].value;
            $squareSlider.data('range', optsForSquareSlider);
        }

        if (statesArr.floor_from) {
            let $floorSlider = $form.find("#floorSlider");
            let optsForFloorSlider = $floorSlider.data('ranges');
            optsForFloorSlider.valueMin = statesArr.floor_from[0].value;
            optsForFloorSlider.valueMax = statesArr.floor_to[0].value;
            $floorSlider.data('range', optsForFloorSlider);
        }

        {
            let storedCheckboxes = [];
            for (let n of ["bedrooms[]", "house[]", "view_direction[]"]) {
                if (statesArr[n]) storedCheckboxes.push(statesArr[n]);
            }
            storedCheckboxes = _.flatten(storedCheckboxes);

            for (let i = 0; i < storedCheckboxes.length; i++) {
                window.console.log(storedCheckboxes[i]);
                let chk = storedCheckboxes[i];
                $form.find(`[name='${chk.name}'][value='${chk.value}']:first`).parent().trigger('click');
            }
        }

        {
            let storedMultyCheckboxes = [];
            for (let n of ["courtyard", "balcony", "furnish"]) {
                if (statesArr[n]) storedMultyCheckboxes.push(statesArr[n]);
            }
            storedMultyCheckboxes = _.flatten(storedMultyCheckboxes);

            for (let i = 0; i < storedMultyCheckboxes.length; i++) {
                let chk = storedMultyCheckboxes[i];
                $form.find(`[name='${chk.name}']`).val(chk.value);
            }
        }

    };

    restoreFiltersParams({
        form: $("#pickupFlatFilterForm"),
        storageID: "filterFormParams",
    });

    let setMinMaxSliderTitles = (event, ui) => {
        window.requestAnimationFrame(() => {
            event.target.getElementsByClassName('min')[0].innerHTML = ui.values[0];
            event.target.getElementsByClassName('max')[0].innerHTML = ui.values[1];
        });
    };

    let setRangeInputsValues = (event, ui) => {

        let $inputs = $(event.target.getElementsByTagName('input'));
        for (var i = 0; i < 2; i++) {
            let $inp = $inputs.eq(i);
            let oldVal = $inp.val();
            let newVal = ui.values[i];

            if (oldVal != newVal) {
                $inp.val(newVal).trigger('change');
            }
        }
    };



    let filterTopbarTpl = _.template($newView.find("#filterTopBarTemplate").html());

    let renderFilterTopbar = () => {
        let context = {
            floor_from: null,
            floor_to: null,
            area_from: null,
            area_to: null,
            bedrooms: [],
            house: [],
            view_direction: [],
            furnish: null,
            balcony: null,
            courtyard: null,
        };



        $("#pickupFlatFilterForm input").each((index, el) => {
            if (el.className.includes("b-multycheckbox__input")) { // multiple checkboxes
                if (el.value === "1") context[el.name] = true;
            } else if (_.includes(el.name, "[]")) { // single checkboxes
                if (el.checked) context[_.trimEnd(el.name, "[]")].push(el.parentNode.textContent);
            } else if (el.type === "hidden") { // range sliders
                context[el.name] = el.value;
            }
        });

        $newView.find("#filterResults").html(filterTopbarTpl(context));


    };


    let $simpleSliders = $("#squareSlider, #floorSlider");
    $simpleSliders.each(function(index, el) {
        let $el = $(el);
        let opts = $el.data("ranges");
        $(el)
            .slider({
                range: true,
                values: [opts.valueMin, opts.valueMax],
                min: opts.min,
                max: opts.max,
                slide: setMinMaxSliderTitles,
            }).one('slidechange', setMinMaxSliderTitles)

            .on('slidechange', setRangeInputsValues)

            .slider("values", [opts.valueMin, opts.valueMax]);
    });


    let $completenessSlider = $("#completenessSlider");
    let firstRelease = new Date($completenessSlider.data("slider-min") * 1000);
    let lastRelease = new Date($completenessSlider.data("slider-max") * 1000);


    let getQuartersFromDateRange = (start, end) => {
        let result = [{
            utc: 0,
            title: "сдан",
        }];
        let quarterPoints = [moment(start).unix()];
        let tmpQuarterPoint = new Date(start);

        while (tmpQuarterPoint < end) {
            tmpQuarterPoint.setMonth(tmpQuarterPoint.getMonth() + 3);
            quarterPoints.push(moment(tmpQuarterPoint).unix());
        }

        let fmtTpl = 'Qкв. YYYY';

        let titles = [];
        for (let i = 0; i < quarterPoints.length; i++) {
            let romanNumbers = [
                "I",
                "II",
                "III",
                "IV",
            ];
            let titleStr = moment.unix(quarterPoints[i]).format(fmtTpl);
            let quartIndex = parseInt(titleStr) - 1;
            titleStr = romanNumbers[quartIndex] + titleStr.slice(1, titleStr.lenght);
            titles.push(titleStr);
        }

        for (var i = 0; i < titles.length; i++) {
            result.push({
                utc: quarterPoints[i],
                title: titles[i],
            });

        }
        return result;
    };

    let quarterPoints = getQuartersFromDateRange(firstRelease, lastRelease);

    let setMinMaxQuarters = (event, ui) => {
        window.requestAnimationFrame(() => {
            event.target.getElementsByClassName('min')[0].innerHTML = quarterPoints[ui.values[0]].title;
            event.target.getElementsByClassName('max')[0].innerHTML = quarterPoints[ui.values[1]].title;
        });
    };

    $completenessSlider
        .slider({
            range: true,
            min: 0,
            max: quarterPoints.length - 1,
            step: 1,
            values: [0, quarterPoints.length - 1],
            slide: setMinMaxQuarters,
        })
        .one('slidechange', setMinMaxQuarters)

        .on('slidechange', (event, ui) => {

            let $inputs = $(event.target.getElementsByTagName('input'));
            for (var i = 0; i < 2; i++) {
                let $inp = $inputs.eq(i);
                let oldVal = $inp.val();
                let newVal = quarterPoints[ui.values[i]].utc;

                if (oldVal != newVal) {
                    $inp.val(newVal).trigger('change');
                }
            }
        })
        .slider("values", [0, quarterPoints.length - 1]);


    $newView.find('#pickupFlatFilterForm input').on('change', _.debounce(() => {
        renderFilterTopbar();

        if (getScreenSizeBreakpoint() !== "xs") {

            $newView.find('.filtr input[type=submit]:first').trigger('click');
        }
    }, 500));

    let asideFilter = new FlatsFilter($newView.find('.filtr:first').get(0));
    $newView.find('.btn_param:first').on('click', function(event) {
        event.preventDefault();
        asideFilter.open();
    });

    let flatsListTpl = _.template($newView.find("#flatsListTemplate").html());

    $("#pickupFlatFilterForm")
        .on('submit', (event) => {
            event.preventDefault();
            window.pagePreloader.show();
            if (getScreenSizeBreakpoint() === "xs") {
                asideFilter.close();
            }

            let $form = $(event.target);

            let $nonEmptyInputs = $form.find("input").filter(function(index, element) {
                return $(element).val() !== "";
            });

            preserveFiltersParams({
                inputs: $nonEmptyInputs,
                storageID: "filterFormParams",
            });

            let urlParamsStr = $nonEmptyInputs.serialize();
            if (urlParamsStr.length) {
                urlParamsStr = "&" + urlParamsStr;
            }

            $.ajax({
                url: 'http://pirogovo-riviera.ru/flats/filter/?ajax=1&get_typicals=1&filter=1' + urlParamsStr,
            }).done((response) => {

                let markup = flatsListTpl(response);

                let $flatsCont = $newView.find("#flatsListContainer");
                $flatsCont.html(markup);
                $flatsCont.find('[data-toggle="tooltip"]').tooltip();
                window.pagePreloader.hide();
            });
        })
        .trigger('submit');





    window.favList = new FavoritesList(document.getElementById('favoritesFlatsList'));
    window.favList.refresh();
    //window.favList.show();

    $newView.on('click', '[data-toggle-favorite]', function(event) {
        event.preventDefault();
        let el = event.currentTarget;
        let $el = $(el);
        let id = el.dataset.toggleFavorite;

        window.pagePreloader.show();

        $.ajax({
                url: `http://pirogovo-riviera.ru/flats/filter/?favorite=${id}`,
            })
            .always(function(response) {
                let $togglers = $(`[data-toggle-favorite='${id}']`);
                let toggledTitle = response.status === 1 ? "Убрать из избранного" : "Добавить в избранное";

                $el.next(".tooltip:first").find(".tooltip-inner:first").html(toggledTitle);
                $togglers
                    .attr('data-original-title', toggledTitle)
                    .tooltip("fixTitle")
                    .toggleClass('active');

                window.favList.refresh();
                window.pagePreloader.hide();
                setTimeout(() => {
                    $el.trigger('blur')
                        .trigger('mouseleave');
                }, 2000);

                try {
                    gtag('event', 'add_to_favorites', {
                        'event_category': 'click',
                        'event_action': 'add_to_favorites_success'
                    });
                    yaCounter47174739.reachGoal('add_to_favorites_success');
                } catch (err) {
                    console.log(err);
                }
            });
    });

    renderFilterTopbar();

    $newView.one('replace', () => {
        asideFilter.destroy();
        window.favList.destroy();
        window.favList = null;
        $simpleSliders.add($completenessSlider).slider("destroy");
        $newView.find('[data-toggle="tooltip"]').tooltip("destroy");
    });
};
