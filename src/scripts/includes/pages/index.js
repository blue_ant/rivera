window.viewInitializers.homepage = ($newView) => {
    let $slider = $("#homepageActionsSlider .slider-inner:first");
    $slider.slick({
        prevArrow: "#homepageActionsSlider .str_t",
        nextArrow: "#homepageActionsSlider .str_b",
        vertical: true,
        autoplay: true,
        draggable: false,
        swipe: false,
    });

    $slider.slick("goTo", 0);

    let animeState = {
        iter: 0,
    };

    let figureLinesTL = new TimelineMax({ repeat: -1 });
    let linesTL = new TimelineMax({ repeat: -1 });

    let $line1 = $newView.find('#line1-step1');
    let $line2 = $newView.find('#line2-step1');
    let $line3 = $newView.find('#line3-step1');
    let $line4 = $newView.find('#line4-step1');
    let $svgLeaf = $newView.find("#leaf");

    let t = 0.8;

    linesTL
        .addLabel("step2")
        .to($line3, t * 1.5, { morphSVG: "#line3-step2", ease: Linear.easeNone }, "step2")
        .to($line4, t * 1.5, { morphSVG: "#line4-step2", ease: Linear.easeNone }, "step2")
        .addLabel("step3")
        .to($line3, t * 1.5, { morphSVG: "#line3-step3", ease: Linear.easeNone }, "step3")
        .to($line4, t * 1.5, { morphSVG: "#line4-step3", ease: Linear.easeNone }, "step3")
        .addLabel("step4")
        .to($line3, t * 1.5, { morphSVG: "#line3-step4", ease: Linear.easeNone }, "step4")
        .to($line4, t * 1.5, { morphSVG: "#line4-step4", ease: Linear.easeNone }, "step4")
        .addLabel("step5")
        .to($line3, t * 1.5, { morphSVG: "#line3-step5", ease: Linear.easeNone }, "step5")
        .to($line4, t * 1.5, { morphSVG: "#line4-step5", ease: Linear.easeNone }, "step5")
        .addLabel("step6")
        .to($line3, t * 1.5, { morphSVG: "#line3-step6", ease: Linear.easeNone }, "step6")
        .to($line4, t * 1.5, { morphSVG: "#line4-step6", ease: Linear.easeNone }, "step6")
        .addLabel("step7")
        .to($line3, t * 1.5, { morphSVG: "#line3-step7", ease: Linear.easeNone }, "step7")
        .to($line4, t * 1.5, { morphSVG: "#line4-step7", ease: Linear.easeNone }, "step7")
        .addLabel("step8")
        .to($line3, t * 1.5, { morphSVG: "#line3-step8", ease: Linear.easeNone }, "step8")
        .to($line4, t * 1.5, { morphSVG: "#line4-step8", ease: Linear.easeNone }, "step8")
        .addLabel("step9")
        .to($line3, t * 1.5, { morphSVG: "#line3-step9", ease: Linear.easeNone }, "step9")
        .to($line4, t * 1.5, { morphSVG: "#line4-step9", ease: Linear.easeNone }, "step9")
        .addLabel("step10")
        .to($line3, t * 1.5, { morphSVG: "#line3-step10", ease: Linear.easeNone }, "step10")
        .to($line4, t * 1.5, { morphSVG: "#line4-step10", ease: Linear.easeNone }, "step10")
        .addLabel("step11")
        .to($line3, t * 1.5, { morphSVG: "#line3-step11", ease: Linear.easeNone }, "step11")
        .to($line4, t * 1.5, { morphSVG: "#line4-step11", ease: Linear.easeNone }, "step11");

    figureLinesTL
        .addLabel("step1")
        .call(() => {
            if (animeState.iter >= window.animeDynData.titles.length) {
                animeState.iter = 0;
            }
            $newView.find('#slider_main__bg-pic').css({
                backgroundImage: `url(/img/animeBg-xs-${animeState.iter + 1}.jpg)`,
            });

            $newView.find('#sliderHead').html(animeDynData.titles[animeState.iter]);
            $newView.find('#sliderText').html(animeDynData.texts[animeState.iter]);
            $newView.find('#leaf').css({
                fill: `url(#img${animeState.iter + 1})`,
            }, this, "step1");
        })
        .to(["#sliderHead", "#sliderText", "#slider_main__bg-pic"], 0.4, { opacity: 1 }, "step1+=0.1")

    .addLabel("step2")
        .to($line1, t, { morphSVG: "#line1-step2", ease: Linear.easeNone }, "step2")
        .to($line2, t, { morphSVG: "#line2-step2", ease: Linear.easeNone }, "step2")
        .addLabel("step3")
        .to($line1, t, { morphSVG: "#line1-step3", ease: Linear.easeNone }, "step3")
        .to($line2, t, { morphSVG: "#line2-step3", ease: Linear.easeNone }, "step3")
        .addLabel("step4")
        .to($line1, t, { morphSVG: "#line1-step4", ease: Linear.easeNone }, "step4")
        .to($line2, t, { morphSVG: "#line2-step4", ease: Linear.easeNone }, "step4")
        .addLabel("step5")
        .to($line1, t, { morphSVG: "#line1-step5", ease: Linear.easeNone }, "step5")
        .to($line2, t, { morphSVG: "#line2-step5", ease: Linear.easeNone }, "step5")
        .addLabel("step6")
        .call(() => {
            TweenLite.to($line1, t, { morphSVG: `#line1-step6-iter${animeState.iter+1}` }, "step6");
        }, this, "step6")
        .call(() => {
            TweenLite.to($line2, t, { morphSVG: `#line2-step6-iter${animeState.iter+1}` }, "step6");
        }, this, "step6")
        .addLabel("step7", "step6+=2.5")
        .to($line1, t * 1.2, { morphSVG: "#line1-step7" }, "step7")
        .to($line2, t * 1.2, { morphSVG: "#line2-step7" }, "step7")
        .set($svgLeaf, {
            visibility: "visible",
            onComplete: () => {
                TweenLite.to($svgLeaf, t * 2, { morphSVG: `#leaf-final-iter${animeState.iter+1}` });
            }
        }, "step7")

    .addLabel("step8")

    .to($line1, t * 2, { morphSVG: "#line1-step8", ease: Linear.easeNone }, "step8")
        .to($line2, t * 2, { morphSVG: "#line2-step8", ease: Linear.easeNone }, "step8")
        .addLabel("step9")
        .to($line1, t * 2, { morphSVG: "#line1-step9", ease: Linear.easeNone }, "step9")
        .to($line2, t * 2, {
            morphSVG: "#line2-step9",
            ease: Linear.easeNone,
            onComplete: () => {
                TweenLite.to($svgLeaf, t * 2, { morphSVG: "#leaf-initial", delay: t, onComplete: () => { TweenLite.set($svgLeaf, { visibility: "hidden" }); } });
            }
        }, "step9")
        .addLabel("step10")
        .to($line1, t * 2, { morphSVG: "#line1-step10", ease: Linear.easeNone }, "step10")
        .to($line2, t * 2, {
            morphSVG: "#line2-step10",
            ease: Linear.easeNone,

        }, "step10")
        .addLabel("step11")
        .to($line1, t * 2, { morphSVG: "#line1-step11", ease: Linear.easeNone }, "step11")
        .to($line2, t * 2, { morphSVG: "#line2-step11", ease: Linear.easeNone, }, "step11")
        .to(["#sliderHead", "#sliderText", "#slider_main__bg-pic"], 0.4, {
            opacity: 0,
            onComplete: () => {

                animeState.iter += 1;

            }
        });

    $newView.find('.btn_more:first').on('click', function(event) {
        event.preventDefault();
        figureLinesTL.kill();
        TweenLite.to(["#sliderHead", "#sliderText", "#slider_main__bg-pic"], 0.4, {
            opacity: 0,
            onComplete: () => {
                TweenLite.set("#slider_main__bg-pic", {
                    backgroundImage: "none",
                });
            }
        });
        TweenLite.to($line2, 0.4, {
            morphSVG: "#line2-step11",
        });
        TweenLite.to($line1, 0.4, { morphSVG: "#line1-step11" });
        TweenLite.to(TweenLite.to($svgLeaf, 0.4, {
            morphSVG: "#leaf-initial",
            onComplete: () => {
                TweenLite.set($svgLeaf, { visibility: "hidden" });
                animeState.iter += 1;
                figureLinesTL.restart();
            }
        }));

    });

    $newView.one('replace', () => {
        figureLinesTL.kill();
        linesTL.kill();
        linesTL = figureLinesTL = null;

        $slider.slick('unslick');
    });
};
