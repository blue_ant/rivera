window.viewInitializers["hypothec-military"] = ($newView) => {

    let hypothecForm = new InteractiveForm({
        el: "#hypothecForm",
        submitHandler: modalFormHandler,
    });

    $newView.one('replace', (event) => {
        hypothecForm.destroy();
    });
};
