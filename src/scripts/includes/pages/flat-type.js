window.viewInitializers["flat-type"] = ($newView) => {
    let breakpoint = getScreenSizeBreakpoint();
    $newView.find('.apart_chosen_list_tb_r').on('click', function(event) {

        if (breakpoint === "xs") {
            event.preventDefault();
            $("#hd").find('[data-location-modal="callMeBackLocationModal"]:first').trigger('click');
        }
    });

    $newView.find('.apart_chosen_img').on('click', function(event) {
        event.preventDefault();
        if (breakpoint === "xs" || breakpoint === "sm") {
            this.classList.toggle("apart_chosen_img--pseudo-hovered");
        }
    });

    let $flatlist = $newView.find(".b-flats-list-scroller:first");
    if (device.desktop()) {
        let $flatscroller = $flatlist.niceScroll({
            autohidemode: false,
            enablekeyboard: false,
            cursorcolor: "#d99b47",
            cursoropacitymin: 1,
            cursorborder: "none",
            cursorborderradius: "1px",
            background: "#ebebeb",
            cursorwidth: "3px",
            horizrailenabled: false,
        });

        $newView.one('replace', () => {
            $flatscroller.remove();
        });
    }else{
    	$flatlist.css('overflow', 'auto');
    }


};