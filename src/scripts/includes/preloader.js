class Preloader {
    constructor(id) {
        this.el = document.getElementById(id);
        this.el.onclick = (event) => {
            event.preventDefault();
            event.stopPropagation();
            return false;
        };
    }

    show(cb) {
        this.el.classList.add("b-preloader--active");
        if (cb) cb();
    }

    hide(cb) {
        this.el.classList.remove("b-preloader--active");
        if (cb) cb();
    }

        enableBG() {
            this.el.classList.remove("b-preloader--transparent");
        }

        disableBG() {
            this.el.classList.add("b-preloader--transparent");
        }
}
