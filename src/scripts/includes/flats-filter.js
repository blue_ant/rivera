class FlatsFilter {
    constructor(el) {

        let body = document.body;
        let backdrop = document.createElement("div");
        backdrop.className = "modal-backdrop fade";
        backdrop.style.display = "none";

        this.el = el;
        this.backdrop = backdrop;

        backdrop.onclick = () => {
            this.close();
        };

        body.appendChild(backdrop);

        $(window).on('resize.flatsFilter orientationchange.flatsFilter', _.debounce((event) => {
            event.preventDefault();
            if (getScreenSizeBreakpoint() !== "xs") {
                this.close(0);
            }
        }, 150));

    }

    open() {
        let tl = new TimelineLite();
        tl
            .set(document.body, {
                className: "+=modal-open",

            })
            .set(this.backdrop, { display: "block" })
            .set(this.backdrop, { className: "+=in" })
            .addLabel("filterGlide", "+0.5")
            .to(this.el, 0.4, { autoAlpha: 1 }, 0.01, "filterGlide")
            .from(this.el, 0.4, {
                x: "110%",
            }, "filterGlide");
    }

    close(duration = 0.4) {
        let tl = new TimelineLite();
        tl
            .addLabel("filterGlide")
            .to(this.el, duration, { autoAlpha: 0, delay: 0.1 }, "filterGlide")
            .to(this.el, duration, {
                x: "110%",
            }, "filterGlide")
            .set(this.backdrop, { className: "-=in", })
            .set(document.body, {
                className: "-=modal-open",
                delay: duration,
            })
            .set(this.el, { clearProps: "all" })
            .set(this.backdrop, { display: "none", delay: duration });
    }

    destroy() {
        $(window).off(".flatsFilter");
        this.close(0);
        document.body.removeChild(this.backdrop);
    }
}
