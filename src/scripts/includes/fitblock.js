class Fitblock {
    constructor(el) {
        this.outer = el;
        this.inner = el.getElementsByClassName('b-fitblock__inner')[0];
        this.pusher = this.inner.getElementsByClassName('b-fitblock__pusher')[0];
        this.refresh();
    }

    refresh() {
        window.requestAnimationFrame(() => {
            let outer = this.outer;
            let inner = this.inner;
            let pusher = this.pusher;
            let wrapH = outer.offsetHeight;
            let wrapW = outer.offsetWidth;

            const pusherH = pusher.height;
            const pusherW = pusher.width;
            let rel = pusherW / pusherH;
            if (wrapW / pusherW > wrapH / pusherH) {
                pusher.style.width = `${wrapW}px`;
                pusher.style.height = "auto";
                inner.style.marginLeft = `-${wrapW/2}px`;
                inner.style.marginTop = `-${(wrapW/rel)/2}px`;
            } else {
                pusher.style.width = "auto";
                pusher.style.height = `${wrapH}px`;
                inner.style.marginLeft = `-${(wrapH*rel)/2}px`;
                inner.style.marginTop = `-${wrapH/2}px`;
            }

            inner.style.visibility = "visible";
        });


    }
}
