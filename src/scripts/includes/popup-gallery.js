class PopupGallery {

    constructor(slides = []) {
        let el = document.createElement("div");
        el.className = "b-popup-gallery";
        el.innerHTML = `\
<a href="#" class="b-popup-gallery__close-btn"></a>\
<div class="b-popup-gallery__wrap galery_bigImg">\
<div class="b-popup-gallery__slider">${this.__renderSlides(slides)}</div>\
<a href="#" class="b-popup-gallery__arrow b-popup-gallery__arrow--prev galery_str_l" title=""></a>\
<a href="#" class="b-popup-gallery__arrow b-popup-gallery__arrow--next galery_str_r" title=""></a>\
</div>
`;

        document.body.appendChild(el);

        let arrows = el.getElementsByClassName('b-popup-gallery__arrow');

        let $slider = $(el.getElementsByClassName('b-popup-gallery__slider')[0]);
        let $wrap = $slider.parent();
        let sliderOpts = {
            infinite: false,
            prevArrow: arrows[0],
            nextArrow: arrows[1],
            lazyLoad: 'progressive',
        };

        $slider.slick(sliderOpts);

        el.getElementsByClassName('b-popup-gallery__close-btn')[0].onclick = () => {
            this.hide();
        };

        this.el = el;
        this.$slider = $slider;
        this.isOpen = false;
        this.__sliderOpts = sliderOpts;
    }

    show() {
        if (this.isOpen) return false;

        window.requestAnimationFrame(() => {
            disableWindowScrolling();
            this.el.classList.add("b-popup-gallery--active");
            this.isOpen = true;
        });
    }

    hide() {
        if (!this.isOpen) return false;
        window.requestAnimationFrame(() => {
            let body = document.body;
            this.el.classList.remove("b-popup-gallery--active");
            this.stopPlayingVid(this.$slider.find('.b-popup-gallery__slide:first'));
            setTimeout(() => {
                enableWindowScrolling();
                this.isOpen = false;
            }, 400);

        });

    }

    stopPlayingVid($cont) {
        $cont.find("iframe:first").each(function(index, el) {
            setTimeout(() => {
                let player = new Vimeo.Player(el);
                player.pause();
            }, 0);
        });
    }

    fillGallery(slides) {

        this.$slider.slick("unslick");
        this.$slider.off(".videoStopping");
        $(this.el).find('.b-popup-gallery__slider:first').html(this.__renderSlides(slides));
        this.$slider.on('beforeChange.videoStopping', (event, slick, currentSlide) =>{
            if (currentSlide === 0) {
                this.stopPlayingVid(slick.$slides.eq(0));
            }
            //window.console.log(currentSlide);
            
        });
        this.$slider.slick(this.__sliderOpts);

    }




    __renderSlides(slides) {
        let html = "";

        for (let i = 0; i < slides.length; i++) {
            html += `<div class="b-popup-gallery__slide"><div class="b-popup-gallery__picwrap">\
<img class="b-popup-gallery__img" src="/img/throbber.gif" data-lazy="${slides[i]}" alt=""></div></div>`;
        }
        return html;
    }

    destroy() {
        this.$slider.slick("unslick");
        this.show = this.hide = this.fillGallery = null;
        $(this.el).remove();
    }
}

class PopupGalleryWithDetails extends PopupGallery {
    __renderSlides(slides) {
        let html = "";

        for (let i = 0; i < slides.length; i++) {
            let sld = slides[i];
            html += '<div class="b-popup-gallery__slide"><div class="b-popup-gallery__picwrap"><div class="b-popup-gallery__details">';

            if (sld.img) {
                html += `<img class="b-popup-gallery__img b-popup-gallery__img--with-details" src="/img/throbber.gif" data-lazy="${sld.img}" alt="">`;
            } else {
                html += `
<div style="position: relative;display: inline-block;">
    <img class="b-popup-gallery__img b-popup-gallery__img--with-details" src="/img/pushers/16x9.png" alt="">
    <iframe class="b-popup-gallery__video b-popup-gallery__video--with-details" src="${sld.video}"></iframe>
</div>`;
            }

            html += `<div class="b-popup-gallery__white-plate">${sld.title}</div>\
        <div class="b-popup-gallery__bottom-hint">Загружено ${sld.date} ${sld.manager}</div>\
    </div></div></div>`;
        }
        return html;
    }
}
