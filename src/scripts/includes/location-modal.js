$(document).on('click', '[data-location-modal]', (event) => {
    event.preventDefault();

    let modal = document.getElementById(event.currentTarget.dataset.locationModal);
    let inner = modal.getElementsByClassName('b-location-modal__inner')[0];
    let type  = $(modal).attr('id');

    let tl = new TimelineLite();

    if (type === 'singleFlatFormModal' || type === 'callMeBackLocationModal' || type === 'contactsPageFormModal') {
        window.Calltouch.Callback.onClickCallButton();

        return;
    }

    tl
        .call(disableWindowScrolling)
        .set(modal, {
            display: "block",
            opacity: "0",
            className: "+=b-location-modal--active"
        })
        .to(modal, 0.4, {
            opacity: "1",

        })
        .from(inner, 0.4, {
            bottom: `-${inner.offsetHeight}px`
        });

}).on('click', '.b-location-modal__backdrop', function(event) {
    let el = event.target.parentNode;
    let inner = el.getElementsByClassName('b-location-modal__inner')[0];
    let tl = new TimelineLite();
    tl
        .to(inner, 0.4, { bottom: `-${inner.offsetHeight}px` })
        .to(el, 0.4, { opacity: "0" })
        .set(el, {
            className: "-=b-location-modal--active",
        }).set([el, inner], {
            clearProps: "all"
        })
        .call(() => {
            if (document.getElementsByClassName('b-location-modal--active').length === 0) {
                enableWindowScrolling();
                let successTextEl = el.getElementsByClassName('b-form-success')[0];
                let isFormSended = !!successTextEl;

                if (isFormSended) {
                    let $successCont = $(successTextEl);
                    $successCont.prev('.wrap').show();
                    $successCont.remove();
                }
            }
        });
});
