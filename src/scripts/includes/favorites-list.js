class FavoritesList {
    constructor(el) {
        this.el = el;
        this.$el = $(this.el);
        this.$bg = this.$el.find('.favorite_list__backdrop:first');

        if (device.desktop()) {
            this.$scroller = this.$el.find('.favorite_list__scroller:first').niceScroll({
            autohidemode: false,
            enablekeyboard: false,
            cursorcolor: "#d99b47",
            cursoropacitymin: 1,
            cursorborder: "none",
            cursorborderradius: "1px",
            background: "#ebebeb",
            cursorwidth: "3px",
            horizrailenabled: true,
        });

        }
        
        this.$bg.on('click', (event) => {
            event.preventDefault();
            this.hide();
        });


        this.$favCount = $(`<a href="#" class="favorit_link"><i class="favorit_link_count">&nbsp;</i><span>Избранное</span></a>
`);
        $("#hd .logo").after(this.$favCount);
        this.$favCount.on('click', (event) => {
            event.preventDefault();
            this.show();
        });

        this.tpl = _.template(`
<% if(typicals.length) { %>
<div class="favorite_item empty">
    <div class="favorite_item_img"></div>
    <div class="favorite_item_name"></div>
    <div class="favorite_item_row"></div>
    <div class="favorite_item_row"></div>
    <div class="favorite_item_row"></div>
    <div class="favorite_item_row"></div>
    <div class="favorite_item_row"></div>
</div>
<% _.forEach(typicals, function(flat) { %>
<div class="favorite_item">
    <div class="favorite_item_img"><a href="<%= flat.url %>"><img src="<%= flat.img %>" alt=""></a></div>
    <div class="favorite_item_name"><%= flat.name %></div>
    <div class="favorite_item_row"><% if(flat.bedrooms){print(flat.bedrooms + ' комнатная')}else{print('студия')}%></div>
    <div class="favorite_item_row"><% if(flat.area_min === flat.area_max){print(flat.area_min)}else{print(flat.area_min + ' - ' + flat.area_max)} %> м²</div>
    <div class="favorite_item_row"><% if(flat.balcony){print('балкон')}else{print('нет балкона')} %></div>
    <div class="favorite_item_row"><% if(flat.furnish){print('отделка')}else{print('нет отделки')} %></div>
    <div class="favorite_item_row"><% if(flat.courtyard){print('вид во двор')}else{print('-')} %></div>
    <a href="#" data-toggle-favorite="<%= flat.id %>" data-toggle="tooltip" data-placement="left" title="" class="btn_close active" data-original-title="Удалить из избранного"></a>
</div>
<% }); %>

<div class="favorite_item empty">
    <div class="favorite_item_img"></div>
    <div class="favorite_item_name"></div>
    <div class="favorite_item_row"></div>
    <div class="favorite_item_row"></div>
    <div class="favorite_item_row"></div>
    <div class="favorite_item_row"></div>
    <div class="favorite_item_row"></div>
</div>
<% }else{%>
    <div style="padding:70px;" class="lead">Нет квартир в избранном</div>

<% }%>
`);
    }

    destroy() {
        if (this.$scroller) {
            this.$scroller.remove();
        }
        
        enableWindowScrolling();
        this.$favCount.fadeOut(() => {
            this.$favCount.remove();
        });
    }

    refresh() {

        $.ajax({
                url: 'http://pirogovo-riviera.ru/flats/filter/?ajax=1&get_typicals=1&get_favorites=1',
            })
            .done((response) => {
                this.$el.find('.favorite_list_item_tr:first').html(this.tpl(response)).find(["data-toggle='tooltip'"]).tooltip();
                this.$favCount.find(".favorit_link_count:first").html(response.typicals.length);
            });


    }



    show() {

        disableWindowScrolling();
        let tl = new TimelineLite();
        tl.to(this.el, 0.4, {
            autoAlpha: 1,   
        })
        .from(this.$el.find('.favorite_list_inner:first'),0.4,{
            top: "110%",
            onComplete: () => {
                if (this.$scroller) {this.$scroller.resize();}
                
            }
        });

    }

    hide() {
        let tl = new TimelineLite();
        let $inner = this.$el.find('.favorite_list_inner:first');
        tl
        .to($inner,0.3,{
            top: "110%",
        })
        .to(this.el, 0.3, {
            autoAlpha: 0,
        })
        .set($inner, {
            clearProps: "all",
            onComplete: () => {
                enableWindowScrolling();
            }
        });

    }
}
