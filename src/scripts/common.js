/*=require ./includes/google-map-styles.js*/
/*=require ./includes/location-modal.js*/
/*=require ./includes/popup-gallery.js*/
/*=require ./includes/fitblock.js*/
/*=require ./includes/flats-filter.js*/
/*=require ./includes/InteractiveForm.js*/
/*=require ./includes/preloader.js*/
/*=require ./includes/favorites-list.js*/
function getQuartersFromDateRange(start, end) {
    let quarterPoints = [moment(start).unix()];
    let tmpQuarterPoint = new Date(start);
    while (tmpQuarterPoint < end) {
        tmpQuarterPoint.setMonth(tmpQuarterPoint.getMonth() + 3);
        quarterPoints.push(moment(tmpQuarterPoint).unix());
    }
    return quarterPoints;
}

function getScreenSizeBreakpoint() {
    let w = $(window).width();
    let result = "lg";
    if (w < 768) {
        result = "xs";
    } else if (w > 767 && w < 1072) {
        result = "sm";
    }
    return result;
}

function showMainMenu() {
    window.requestAnimationFrame(() => {
        document.body.classList.add("menu-open");
        let $menu = $("#mainMenu");
        let $itemLinks = $menu.find('.menu__item-link');
        let $subItemsLinks = $menu.find('.menu__subitem-link');
        let $wrap = $menu.find('.wrap:first');
        let screenBreakpoint = getScreenSizeBreakpoint();
        let $bg = $menu.find('.menu__backdrop:first');
        $menu.addClass('menu--active');
        $wrap.addClass('wrap--active');
        if (screenBreakpoint == "xs") {
            disableWindowScrolling();
            TweenLite.from($bg, 0.4, {
                opacity: "0"
            });
        } else {
            TweenMax.staggerFrom($itemLinks, 0.4, {
                scale: "0.8",
                y: "200%",
                opacity: "0",
            }, (0.6 / $itemLinks.length));
            TweenLite.from($subItemsLinks, 0.6, {
                y: "100%",
                opacity: "0",
                delay: 0.3
            });
        }
    });
}

function hideMainMenu(cb) {

    let $body = $(document.body);
    if ($body.hasClass('menu-open')) {
        let duration = 0.45;
        let $menu = $("#mainMenu");
        let $bg = $menu.find('.menu__backdrop:first');
        let $wrap = $menu.find('.wrap:first');
        $wrap.removeClass('wrap--active');
        $menu.removeClass('menu--active');
        if (getScreenSizeBreakpoint() === "xs") {
            TweenLite.to($bg, duration, {
                opacity: "0",
                onComplete: () => {
                    TweenLite.set($bg, {
                        clearProps: "all"
                    });
                    $body.removeClass("menu-open");
                    enableWindowScrolling();
                    if (cb) {
                        cb();
                    }
                }
            });
        } else {
            setTimeout(() => {
                $body.removeClass("menu-open");
                enableWindowScrolling();
                if (cb) {
                    cb();
                }
            }, duration * 1000);
        }
    } else {
        if (cb) {
            cb();
        }
    }

}

function centerModals($element) {
    var $modals;
    if ($element.length) {
        $modals = $element;
    } else {
        $modals = $('.modal.in:first');
    }
    $modals.each(function() {
        var $clone = $(this).clone().css('display', 'block').appendTo('body');
        var top = Math.round(($clone.height() - $clone.find('.modal-content').height()) / 2);
        top = top > 0 ? top : 0;
        $clone.remove();
        $(this).find('.modal-content').css("margin-top", top);
    });
}

function updateViewDynamicElementsAndScrollToTop(newPageOpts) {
    let $body = $(document.body);
    if (newPageOpts.isFullSize) {
        $body.addClass('full-size');
    } else {
        $body.removeClass('full-size');
    }
    window.scrollTo(0, 0);
    window.requestAnimationFrame(() => {
        if (newPageOpts.isHeaderBg) {
            $("#hd_bg").removeClass('b-header-bg--hidden');
        } else {
            $("#hd_bg").addClass('b-header-bg--hidden');
        }
        if (newPageOpts.isPageWhite) {
            $("#bd").addClass('white-page');
            $(".b-footer__icons").addClass('b-footer__icons--white');
            $(".b-logo__pic--white").removeClass('b-logo__pic--hidden');
            $(".b-logo__pic--dark").addClass('b-logo__pic--hidden');
        } else {
            $(".b-logo__pic--dark").removeClass('b-logo__pic--hidden');
            $(".b-logo__pic--white").addClass('b-logo__pic--hidden');
            $("#bd").removeClass('white-page');
            $(".b-footer__icons").removeClass('b-footer__icons--white');
        }
    });
}
$(document).on('click', ".menu_btn", function(event) {
    event.preventDefault();
    showMainMenu();
}).on('click', '.b-multycheckbox__btn', function(event) {
    event.preventDefault();
    let btn = event.currentTarget;
    let $inp = $(btn.parentNode.getElementsByTagName('input')[0]);
    let possibleValues = $inp.data("possible-values");
    for (var i = possibleValues.length - 1; i >= 0; i--) {
        possibleValues[i] = possibleValues[i] + "";
    }
    let currentVal = $inp.val() + "";
    let currentValIndex = _.indexOf(possibleValues, currentVal);
    let newVal;
    if (currentValIndex === possibleValues.length - 1) {
        newVal = possibleValues[0];
    } else {
        newVal = possibleValues[currentValIndex + 1];
    }
    $inp.val(newVal).trigger('change');
});
$("#mainMenu").on('click', '.menu__item-link', (event) => {
    let $link = $(event.currentTarget);
    let $wrap = $link.parents(".wrap:first");
    $wrap.addClass('wrap--moving');
    let $subMenu = $link.next("ul");
    if ($subMenu.length) {
        event.preventDefault();
        $subMenu.parent().addClass("active");
        let screenBreakpoint = getScreenSizeBreakpoint();
        if (screenBreakpoint === "xs") {
            let $links = $(".menu__item-link");
            let shift = $links.width() + 34;
            TweenLite.from($subMenu, 0.4, {
                x: `${shift}px`,
                ease: Power2.easeInOut,
            });
            TweenLite.to($links, 0.4, {
                x: `-${shift}px`,
                opacity: "0",
                ease: Power2.easeInOut,
                onComplete: () => {
                    TweenLite.set($links, {
                        clearProps: "all"
                    });
                    $wrap.removeClass('wrap--moving');
                }
            });
        } else {
            $wrap.removeClass('wrap--moving');
        }
    } else {
        $wrap.removeClass('wrap--moving');
    }
}).on('click', '.back > a', (event) => {
    event.preventDefault();
    let $activeItem = $("#mainMenu").find('.item.active:first');
    let $subMenu = $activeItem.find("ul:first");
    let $wrap = $activeItem.parents(".wrap:first");
    $wrap.addClass('wrap--moving');
    if (getScreenSizeBreakpoint() === "xs") {
        let $links = $(".menu__item-link");
        let shift = $links.width() + 34;
        TweenLite.from($links, 0.4, {
            x: `-${shift}px`,
            ease: Power2.easeInOut,
        });
        TweenLite.to($subMenu, 0.4, {
            x: `${shift}px`,
            opacity: "0",
            ease: Power2.easeInOut,
            onComplete: () => {
                $activeItem.removeClass('active');
                TweenLite.set($subMenu, {
                    clearProps: "all"
                });
                $wrap.removeClass('wrap--moving');
            }
        });
    } else {
        $activeItem.removeClass('active');
        $wrap.removeClass('wrap--moving');
    }
}).find('.close:first').on('click', (event) => {
    event.preventDefault();
    hideMainMenu();
});
$(".menu__backdrop:first").on('touchmove scroll', function(event) {
    event.preventDefault();
    return false;
});
window.viewInitializers = {};
/*=require ./includes/pages/*.js */

function applyNewView($cont, $newView, $oldView) {
    let $locationModals = $(".b-location-modal--active");
    if ($locationModals.length) {
        $locationModals.each(function(index, el) {
            $(el.getElementsByClassName('b-location-modal__backdrop')[0]).trigger('click');
        });
    }
    $oldView.trigger('replace');
    $cont.empty().append($newView);
    let pageType = $newView.data('page-vars').pageType;
    let body = document.body;
    body.classList.remove(`l-body--${$oldView.data('page-vars').pageType}`);
    body.classList.add(`l-body--${pageType}`);
    let initFunc = window.viewInitializers[pageType];
    if (initFunc) {
        initFunc($newView);
    }
}

function switchScreen(opts) {
    let fadeDuration = 0.4;
    let manualSettedDuration = window.app.currentAppStates.nextFadeDuration;
    if (manualSettedDuration !== undefined && manualSettedDuration !== null) {
        fadeDuration = manualSettedDuration;
    }
    window.pagePreloader.show();
    $.ajax({
        url: opts.path,
        dataType: 'html',
        data: {
            ajaxpage: 1
        },
    }).always((data) => {
        let $cont = $("#mainView");
        let $oldView = $cont.find('.b-page-content:first');
        let $newView;
        if (data.responseText) {
            $newView = $(data.responseText).find('.b-page-content:first');
        } else {
            $newView = $(data).find('.b-page-content:first');
        }

        $newView.waitForImages({
            finished: function() {
                let oldPageOpts = $oldView.data('page-vars');
                let newPageOpts = $newView.data('page-vars');
                document.title = newPageOpts.title;


                if (getScreenSizeBreakpoint() !== "xs" && $("#mainMenu").hasClass('menu--active')) {
                    updateViewDynamicElementsAndScrollToTop(newPageOpts);
                    applyNewView($cont, $newView, $oldView);
                    hideMainMenu();
                    window.pagePreloader.hide();
                } else {
                    hideMainMenu(() => {
                        if (
                            newPageOpts.pageType === "choice" &&
                            oldPageOpts.pageType === "genplan" &&
                            getScreenSizeBreakpoint() !== "xs"
                        ) {

                            window.pagePreloader.disableBG();
                            let $genplanHalf = $newView.find('.choice_gp_link:first');

                            let transitTL = new TimelineLite();
                            transitTL
                                .set("#bd", {
                                    "background-image": "url(/img/genplan.jpg)",
                                    "background-size": "cover",
                                    "background-position": "center",
                                })
                                .set($genplanHalf, {
                                    transition: "none",
                                    className: "+=b-halfpage--active",
                                    zIndex: 1000,
                                })
                                .to($oldView.find('.point,.point_ready, .progress, .h1_full, .btn_orange, .genplan__compass'), 0.4, {
                                    opacity: "0",
                                })

                            .call(() => {
                                $oldView.trigger('replace');
                                $cont.empty().append($newView);
                                updateViewDynamicElementsAndScrollToTop(newPageOpts);
                            })

                            .set($genplanHalf, {
                                clearProps: "transition",
                            }, "+=0.1")

                            .from($newView.find('.b-halfpage__overlay'), 0.4, {
                                opacity: "0",
                            })

                            .set($genplanHalf, {
                                className: "-=b-halfpage--active",
                            })

                            .from($newView.find('.b-halfpage__link'), 0.4, {
                                opacity: "0",
                            }, "+=0.42")

                            .call(() => {
                                window.viewInitializers.choice($newView);
                                window.pagePreloader.hide(() => {
                                    window.pagePreloader.enableBG();
                                });
                            }).set(["#bd", $genplanHalf], {
                                clearProps: "all",
                            }, "+=0.01");
                        } else {
                            TweenLite.to($cont, fadeDuration, {
                                opacity: "0",
                                onComplete: () => {
                                    applyNewView($cont, $newView, $oldView);
                                    updateViewDynamicElementsAndScrollToTop(newPageOpts);
                                    TweenLite.to($cont, fadeDuration, {
                                        opacity: "1"
                                    });
                                    window.app.currentAppStates.nextFadeDuration = null;
                                    window.pagePreloader.hide();
                                }
                            });
                        }
                    });
                }
            },

            waitForAll: true
        });

    });
}

$(document).ready(function() {

    window.pagePreloader = new Preloader("pagePreloader");
    (() => {
        let $cont = $(".b-page-content:first");
        let initialPageType = $cont.data("page-vars").pageType;
        let startFunction = window.viewInitializers[initialPageType];
        if (startFunction) {
            startFunction($cont);
        }
    })();
    window.app = Sammy();
    window.app.currentAppStates = {
        nextFadeDuration: null
    };
    app.run();
    app.get('(.*)', function() {
        switchScreen({
            path: this.params.splat,
        });
        try {
            ga('set', 'page', this.path);
            ga('send', 'pageview');
            yaCounter47174739.hit("http://" + window.location.hostname + this.path);
        } catch (err) {
            console.log(err);
        }
    });
    new InteractiveForm({
        el: "#callMeForm",
        submitHandler: modalFormHandler
    });

    window.pagePreloader.hide();

    new InteractiveForm({
        el: "#installmentForm",
        submitHandler: modalFormHandler,
    });
});

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

var payment = getUrlParameter('utm_source');

function modalFormHandler(form) {
    let $form  = $(form);
    let ishypo = $form.is('#hypothecForm');
    let isinst = $form.is('#installmentForm');
    let data   = $form.serialize() + '&sessionId=' + window.call_value;
    let phone  = $form.find('input[type=tel]').val().replace(/[^0-9\+]/gim, '');

    window.pagePreloader.show();

    if (ishypo) {
        if (payment != undefined) {
            data = data + '&utm_source=' + payment;
        }
    }

    $.ajax({
        url  : form.action,
        type : form.method,
        data : data,
    }).done((response) => {
        let errorCode = parseInt(response.code);

        if (errorCode === 0) {
            let successText = `\
                <div class="wrap b-form-success">\
                <div class="row thanks">\
                    <div class="h1"><div class="text-center">${response.success}</div></div>\
                    <span class="btn_orange">Спасибо</span>\
                </div>
                </div>`;

            window.requestAnimationFrame(() => {
                $form.parent().hide().after(successText);
            });

            setTimeout(() => {
                $form.trigger('reset').parents(".b-location-modal--active:first").find(".b-location-modal__backdrop:first").trigger('click');
                $form.data('validator').resetForm();
                $form.find('.error,.valid').removeClass('error valid');
            }, 2100);

            if (ishypo || isinst) {
                ctSendCallbackRequest(phone);
            }

            if (ishypo) {
                yaCounter47174739.reachGoal('Ipoteka');
                gtag('event', 'Obratni_zvonok', {'event_category': 'Ipoteka', 'event_action': 'click4', });
                fbq('track', 'Lead');
            }

        } else if (errorCode === 1) {
            let validator = $form.data('validator');

            $form.find('[name="captcha"]:first').val("").trigger('focus');
            $form.find('img:first').trigger('click');
            validator.showErrors({
                captcha: response.error,
            });
        }
    }).always(function() {
        window.pagePreloader.hide();
    });
}
