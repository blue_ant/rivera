'use strict';

$(function() {
    function initKeyShotXR() {
        keyshotXR = new keyshotXR(nameOfDiv,folderName,viewPortWidth,viewPortHeight,backgroundColor,uCount,vCount,uWrap,vWrap,uMouseSensitivity,vMouseSensitivity,uStartIndex,vStartIndex,minZoom,maxZoom,rotationDamping,downScaleToBrowser,addDownScaleGUIButton,downloadOnInteraction,imageExtension,showLoading,loadingIcon,allowFullscreen,uReverse,vReverse,hotspots);
    }

    initKeyShotXR();

    var clickTimer;
    var init = false;

    $('[data-control]').mousedown(function() {
        let $this       = $(this);
        let role        = $this.data('control');
        let $image      = $('#backbuffer');

        if (init) return;

        clickTimer = setInterval(function() {
            init = true;

            if (role === 'plus' || role === 'minus') {
                let matrixRegex = /matrix\((-?\d*\.?\d+),\s*0,\s*0,\s*(-?\d*\.?\d+),\s*0,\s*0\)/;
                let matches     = $image.css('-webkit-transform').match(/-?[\d\.]+/g);
                let scale       = (role === 'plus') ? (Number(matches[0]) + .1) : (Number(matches[0]) - .1);

                $('[data-control]').removeClass('_disabled');

                if (role === 'plus') {
                    if (scale > 3) {
                        $this.addClass('_disabled');

                        return;
                    }
                } else if (role === 'minus') {
                    if (scale < 1.1) {
                        $this.addClass('_disabled');
                    }

                    if (scale < 1) {
                        return;
                    }
                }

                $image.attr('data-scale', scale).css({
                    '-webkit-transform': 'scale(' + scale + ')',
                    'transform' : 'scale(' + scale + ')',
                    '-o-transform': 'scale(' + scale + ')',
                    '-moz-transform': 'scale(' + scale + ')',
                });

                return;
            } else if (role === 'left' || role === 'right') {
                let src = $image.attr('src');

                String.prototype.filename = function(extension) {
                    var s = this.replace(/\\/g, '/');
                    s = s.substring(s.lastIndexOf('/') + 1);
                    return extension ? s.replace(/[?#].+$/, '') : s.split('.')[0];
                }

                let current = Number(src.filename().split('_')[1]);

                if (role === 'left') {
                    if (current >= 1) {
                        $image.attr('src', '/3d/' + folderName + '/0_' + (current - 1) + '.png');
                    } else {
                        $image.attr('src', '/3d/' + folderName + '/0_' + (uCount - 1) + '.png');
                    }
                } else if (role === 'right') {
                    if (current < (uCount - 1)) {
                        $image.attr('src', '/3d/' + folderName + '/0_' + (current + 1) + '.png');
                    } else {
                        $image.attr('src', '/3d/' + folderName + '/0_0.png');
                    }
                }
            }
        }, 100);
    });

    $('[data-control]').mouseup(function() {
        setTimeout(function() {
            clearInterval(clickTimer);
            init = false;
        }, 100);
    });
});
